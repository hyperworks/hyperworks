# Read Me! #

This readme documents important information about general use of the Hyperworks Git system. For information about specific items, you should visit the Hyperworks Wiki.

# Making a Commit #

If you own write privileges to the Hyperworks Git system, here is how you commit:

Note beforehand: If you haven't already, please set up a local Git repo. For more information on setting up Git, go [here](https://confluence.atlassian.com/bitbucket/set-up-git-744723531.html)

### Command Line ###

Firstly, you want to enter the following text into a terminal window (i.e Command Prompt)

**git add yourfile.eg**

Alternatively, you can go to the physical repository and copy and paste the file.

Next you want to commit the file:

**git commit -m "A message here"**

That's it!

# GUI-based client (i.e Atlassian SourceTree) #

Firstly, you want to put in the file into the repository (go to Command Line).

Next, you want to commit. In SourceTree, there should be a button that says Commit.

For more enquiries: adrian(dot)marcus(dot)widjaja(at)gmail(dot)com


