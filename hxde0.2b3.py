from Tkinter import *
import tkFileDialog
import tkFileDialog
import tkMessageBox
from tkFileDialog import askopenfile

#Runs on Python 2.7 ONLY! (I think)
#maybe someone will change it so it works on Python3
#I only use 2.7 because it's easier for me

#I am sad because Stex is better than hxDE :(

root=Tk("hxDE")

def track_change_to_text(event):
    text.tag_add("here", "1.0", END)
    text.tag_config("here", background="white", foreground="blue")

root.title("hxDE - Hyperworks Developement Environment")
text=Text(root)
root.grid_columnconfigure(0, weight=1)
root.grid_rowconfigure(1, weight=1)

text.grid(row=1, column=0, columnspan=4, sticky=N+S+W+E)
text.configure(background='#FFFFFF')

text.insert(INSERT, "hxDE 0.2 build 003 (penguin) on darwin.")
text.insert(END, " Visit https://bitbucket.org/hyperworks/hyperworks/wiki/hxDE for more information.")

text.bind('<KeyPress>', track_change_to_text)

filename = None

w = Label(root, text="hxDE 0.2 codename Penguin", font=("Helvetica", 15), fg="black")
w.grid()


    


def saveas():

    global text

    t = text.get("1.0", "end-1c")

    savelocation=tkFileDialog.asksaveasfilename()

    file1=open(savelocation, "w+")

    file1.write(t)

    file1.close()




def open_command():
    
    global filename
    text.delete('1.0', END)
    filename = askopenfile(filetypes=[("Python files","*.py")])
    txt = filename.read()
    text.insert(END, txt)
    filename.close()
    
        
def about_command():
    label = tkMessageBox.showinfo("hxDE 0.2 Alpha", "hxDE 0.2 Alpha, Codename Penguin \nProgrammed by Adrian \n Open Source Software")

def magic():
    m = Label(root, text="You pressed the button", font=("Avenir Next", 15), fg="black")
    m.grid()
def save():
    txt=text.get('1.0','end-1c')
    filepath=open(filename,'w')
    filepath.write(txt)
    filepath.close()

     
                   
    
    
    
   
    

    
    

menu = Menu(root)
root.config(menu=menu)
filemenu = Menu(menu)
menu.add_cascade(label="File", menu=filemenu)
filemenu.add_command(label="Save", command=save)
filemenu.add_command(label="Save As...", command=saveas)
filemenu.add_command(label="Open...", command=open_command) #working now
filemenu.add_command(label="Don't press this button", command=magic) #wasn't working before
helpmenu = Menu(menu)
menu.add_cascade(label="Help", menu=helpmenu)
helpmenu.add_command(label="About hxDE...", command=about_command)


root.mainloop()



#Open Source Software. Everyone loves open source!
#hxDE was programmed by Adrian Widjaja. 
#
#
#